package com.codecampos.twotruthandalie.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.codecampos.twotruthandalie.R
import com.codecampos.twotruthandalie.custom.VerticalViewPager
import com.squareup.picasso.Picasso

/**
 * Created by marcos on 5/31/18.
 */
class ProfileImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_image_pager)

        val pager = findViewById<VerticalViewPager>(R.id.view_pager)

        if (intent.extras != null) {
            val listOfImages = intent.getStringArrayListExtra("IMAGES")
            val imageAdapter = ImageAdapter(supportFragmentManager, listOfImages)
            pager.adapter = imageAdapter
        }
    }

    class ImageAdapter(fragmentManager: FragmentManager, val images : List<String>) : FragmentStatePagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment {
            return ImageFragment.newInstance(images[position])
        }

        override fun getCount(): Int {
            return images.count()
        }

    }

    class ImageFragment : Fragment() {

        companion object{
            private val IMAGE_URL = "imageURL"

            fun newInstance(imageUrl: String) : ImageFragment {
                val args: Bundle = Bundle()
                args.putString(IMAGE_URL, imageUrl)
                val fragment = ImageFragment()
                fragment.arguments = args
                return fragment
            }
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

            val view = inflater.inflate(R.layout.profile_image_item, container, false)

            val image = view.findViewById<ImageView>(R.id.image_profile)
            Picasso.get().load(arguments?.getString(IMAGE_URL)).into(image)

            return view
        }

    }
}