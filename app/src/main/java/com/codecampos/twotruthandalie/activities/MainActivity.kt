package com.codecampos.twotruthandalie.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.codecampos.twotruthandalie.R
import com.codecampos.twotruthandalie.custom.VerticalViewPager
import com.codecampos.twotruthandalie.fragments.QuoteFragment
import com.codecampos.twotruthandalie.data.Mock
import com.codecampos.twotruthandalie.data.ProfileDao

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_pager)

        val pager = findViewById<VerticalViewPager>(R.id.view_pager)

        val mock = Mock();

        val imageAdapter = QuoteAdapter(supportFragmentManager, mock.mockUsers())
        pager.adapter = imageAdapter

    }

    class QuoteAdapter(fragmentManager: FragmentManager, val profile : List<ProfileDao>) : FragmentStatePagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment {
            return QuoteFragment.newInstance(profile[position])
        }

        override fun getCount(): Int {
            return profile.count()
        }

    }
}
