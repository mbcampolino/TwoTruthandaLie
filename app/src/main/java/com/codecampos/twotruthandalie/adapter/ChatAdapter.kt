package com.codecampos.twotruthandalie.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.codecampos.twotruthandalie.R
import com.codecampos.twotruthandalie.data.ChatDao
import kotlinx.android.synthetic.main.item_chat.view.*
import java.util.zip.Inflater

/**
 * Created by marcos on 6/16/18.
 */
class ChatAdapter(chat : List<ChatDao>) : RecyclerView.Adapter<ChatAdapter.ChatHolder>() {

    val chat : List<ChatDao>

    init {
        this.chat = chat
    }

    override fun onBindViewHolder(holder: ChatHolder, position: Int) {

        val chatDao = chat[position]

        if (chatDao.id_user == "0") {
            holder.textMessage?.setBackgroundResource(R.drawable.chat_background_him)
        } else {
            holder.textMessage?.setBackgroundResource(R.drawable.chat_background_her)
        }

        holder.textMessage?.text = chatDao.message
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder {
        return ChatHolder(View.inflate(parent.context, R.layout.item_chat, null))
    }

    override fun getItemCount(): Int {
        return chat.size
    }


    class ChatHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val textMessage = itemView?.findViewById<TextView>(R.id.text_chat)
    }
}