package com.codecampos.twotruthandalie.data

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by marcos on 5/31/18.
 */
class QuoteDao(answer: String, isATrue: Boolean) : Parcelable {
    val answer: String

    val isATrue: Boolean

    init {
        this.answer = answer
        this.isATrue = isATrue
    }

    constructor(source: Parcel) : this(
            source.readString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(answer)
        writeInt((if (isATrue) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<QuoteDao> = object : Parcelable.Creator<QuoteDao> {
            override fun createFromParcel(source: Parcel): QuoteDao = QuoteDao(source)
            override fun newArray(size: Int): Array<QuoteDao?> = arrayOfNulls(size)
        }
    }
}