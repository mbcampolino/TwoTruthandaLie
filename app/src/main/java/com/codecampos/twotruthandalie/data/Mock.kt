package com.codecampos.twotruthandalie.data

/**
 * Created by marcos on 6/13/18.
 */
class Mock {

    fun mockUsers() : List<ProfileDao> {
        val listOfUsers = arrayListOf<ProfileDao>()

        val listOfImages0 = arrayListOf<String>(
                "http://querodicas.com.br/wp-content/uploads/2018/04/img_5ade676c3682f.png")
        val listOfImages1 = arrayListOf<String>(
                "http://k32.kn3.net/taringa/7/2/4/E/B/A/0Toni0/655.jpg",
                "https://i.pinimg.com/originals/56/d4/d7/56d4d7ad4ebaf1f4852e87f45aa0a465.jpg")
        val listOfImages2 = arrayListOf<String>(
                "https://flashbrazil.com/wp-content/uploads/2017/08/Fotos-Fake-%E2%80%93-Download-34-500x430.jpg",
                "http://4.bp.blogspot.com/-l9L2CpLp2xM/U5NQ61H3N4I/AAAAAAAAADc/T4b-q6jK9Tw/s1600/533500_339256436173268_314553622_n.jpg",
                "http://querodicas.com.br/wp-content/uploads/2018/04/img_5ade67606bf6d.png")
        val listOfImages3 = arrayListOf<String>(
                "https://seguraopicuma.files.wordpress.com/2010/05/img_1856.jpg")
        val listOfImages4 = arrayListOf<String>(
                "https://papoativo.com/wp-content/uploads/2013/12/304396_355789981174874_1499418371_n1-419x420.jpg",
                "https://i.pinimg.com/originals/6d/bd/1a/6dbd1af9b69b6629576ade7700dfee8b.jpg",
                "http://4.bp.blogspot.com/-zCs70I87zYU/UMtHre0zUtI/AAAAAAAAAto/sq4VBBIPgw8/s1600/RAAAAL6N_EG4TSviVRH2jlCw82vMh9uS583b1GRQKxp0ViQdo5ppbOo9aPUWSb_JWF6sQZM8bOtcXz9Zrnm-esQHCu3NQimNyqfA9Q2A6hwZTyLJAJtU9VCdPDq9Qhygvh0cPp8BJartm0m55g.jpg")


        val quote1 = arrayListOf<QuoteDao>(
                QuoteDao("jogadora de basquete", true),
                QuoteDao("gosto de funk", false),
                QuoteDao("moro sozinha", true))
        val quote2 = arrayListOf<QuoteDao>(
                QuoteDao("estudante de direito", true),
                QuoteDao("baterista profissional", false),
                QuoteDao("snowboard forever", true))
        val quote3 = arrayListOf<QuoteDao>(
                QuoteDao("harry potter forever", true),
                QuoteDao("maratonista", false),
                QuoteDao("vagal", true))
        val quote4 = arrayListOf<QuoteDao>(
                QuoteDao("viajante", true),
                QuoteDao("um tanto nerd", true),
                QuoteDao("gosto de cozinhar", false))
        val quote5 = arrayListOf<QuoteDao>(
                QuoteDao("meu nome talvez seja Bianca", false),
                QuoteDao("sou vegetariana", true),
                QuoteDao("adoro o por do sol", true))


        listOfUsers.add(ProfileDao("Rebeca", "Santo Amaro", "SP", "09-11-1992", listOfImages0, quote1))
        listOfUsers.add(ProfileDao("Julia", "Santo Amaro", "SP", "09-11-1992", listOfImages1, quote2))
        listOfUsers.add(ProfileDao("Amanda", "Jardins", "SP", "09-11-1996", listOfImages2, quote3))
        listOfUsers.add(ProfileDao("Joyce", "Pinheiros", "SP", "09-11-1991", listOfImages3, quote4))
        listOfUsers.add(ProfileDao("Vivi", "Franco da Rocha", "SP", "09-11-1995", listOfImages4, quote5))

        return  listOfUsers
    }


    fun mockChat() : List<ChatDao> {

        val chatList = arrayListOf<ChatDao>()

        chatList.add(ChatDao("Oiiii","2018-08-19 12:11","1"))
        chatList.add(ChatDao("e entao...o que voce acha que eu estou mentindo?","2018-08-19 12:11","1", mockUsers().get(0).getArrayQuotes()))
        chatList.add(ChatDao("gosto de funk","2018-08-19 12:11","0"))
        chatList.add(ChatDao("eiii isso e verdade :/","2018-08-19 12:11","1"))
        chatList.add(ChatDao("mas tudo bem...o que voce gosta de fazer?","2018-08-19 12:11","1", arrayListOf("sair e beber","ver um filme","viajar")))
        chatList.add(ChatDao("ver um filme","2018-08-19 12:11","0"))
        chatList.add(ChatDao("hmmm que legal eu tambem gosto muito de filmes","2018-08-19 12:11","1"))
        chatList.add(ChatDao("e musica? O que mais gosta de escutar?","2018-08-19 12:11","1", arrayListOf("rock","sertanejo","eletronica", "funk")))
        chatList.add(ChatDao("rock","2018-08-19 12:11","0"))
        chatList.add(ChatDao("eu adoro rock! estou ocupada agora depois conversamos ;)","2018-08-19 12:11","1"))

        return chatList

    }
}