package com.codecampos.twotruthandalie.data

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by marcos on 5/31/18.
 */

class ChatDao(message: String, date: String, id_user: String, question : List<String> = arrayListOf()) : Parcelable {

    val message: String
    val date: String
    val id_user: String
    val question : List<String>

    init {
        this.message = message
        this.date = date
        this.id_user = id_user
        this.question = question
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.createStringArrayList()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(message)
        writeString(date)
        writeString(id_user)
        writeStringList(question)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ChatDao> = object : Parcelable.Creator<ChatDao> {
            override fun createFromParcel(source: Parcel): ChatDao = ChatDao(source)
            override fun newArray(size: Int): Array<ChatDao?> = arrayOfNulls(size)
        }
    }
}