package com.codecampos.twotruthandalie.data

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by marcos on 5/31/18.
 */

class ProfileDao(name: String, city: String, state: String, birthday: String, images: List<String>, quotes: List<QuoteDao>) : Parcelable {

    val name: String
    val city: String
    val state: String
    val birthday: String
    val images: List<String>
    val quotes: List<QuoteDao>

    init {
        this.name = name
        this.city = city
        this.state = state
        this.birthday = birthday
        this.images = images
        this.quotes = quotes
    }

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.createStringArrayList(),
            source.createTypedArrayList(QuoteDao.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(name)
        writeString(city)
        writeString(state)
        writeString(birthday)
        writeStringList(images)
        writeTypedList(quotes)
    }

    fun getArrayQuotes() : ArrayList<String> {
        var quote : ArrayList<String> = arrayListOf()

        for (i in 0..quotes.size-1) {
            quote.add(quotes[i].answer)
        }

        return quote
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProfileDao> = object : Parcelable.Creator<ProfileDao> {
            override fun createFromParcel(source: Parcel): ProfileDao = ProfileDao(source)
            override fun newArray(size: Int): Array<ProfileDao?> = arrayOfNulls(size)
        }
    }
}