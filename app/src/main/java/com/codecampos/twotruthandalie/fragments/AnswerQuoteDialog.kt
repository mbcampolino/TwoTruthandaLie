package com.codecampos.twotruthandalie.fragments

import android.app.Dialog
import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.codecampos.twotruthandalie.R
import com.codecampos.twotruthandalie.data.ProfileDao
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import android.widget.FrameLayout
import com.codecampos.twotruthandalie.adapter.ChatAdapter
import com.codecampos.twotruthandalie.data.Mock


/**
 * Created by marcos on 6/16/18.
 */
class AnswerQuoteDialog : BottomSheetDialogFragment() {

    companion object{
        private val PROFILE_QUOTE = "profileQuote"

        fun newInstance(profile: ProfileDao) : AnswerQuoteDialog {
            val args: Bundle = Bundle()
            args.putParcelable(PROFILE_QUOTE, profile)
            val fragment = AnswerQuoteDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        val view = View.inflate(context, R.layout.dialog_answer_quote, null)
        dialog.setContentView(view)

        val bottomSheet = dialog.window.findViewById<FrameLayout>(android.support.design.R.id.design_bottom_sheet)
        bottomSheet.setBackgroundResource(R.drawable.dialog_background_corner)

        val params = (view.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior

        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)
        }

        val dao = arguments?.getParcelable<ProfileDao>(PROFILE_QUOTE) as ProfileDao;

        val imageProfile = view.findViewById<CircleImageView>(R.id.image_view_profile)
        val txName = view.findViewById<TextView>(R.id.tx_name)
        Picasso.get().load(dao.images[0]).into(imageProfile)
        txName.text = dao.name

        val chatView = view.findViewById<RecyclerView>(R.id.recycler_view)
        chatView.layoutManager = LinearLayoutManager(activity)
        chatView.setHasFixedSize(true)
        chatView.addItemDecoration(ItemDecoration(16))

        val mock = Mock()
        chatView.adapter = ChatAdapter(mock.mockChat())
    }

    inner class ItemDecoration(private val verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.bottom = verticalSpaceHeight
            outRect.left = verticalSpaceHeight
            outRect.right = verticalSpaceHeight
        }
    }

    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }

        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }
}