package com.codecampos.twotruthandalie.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.codecampos.twotruthandalie.*
import com.codecampos.twotruthandalie.activities.ProfileImageActivity
import com.codecampos.twotruthandalie.data.ProfileDao
import com.squareup.picasso.Picasso
import java.util.ArrayList

/**
 * Created by marcos on 6/1/18.
 */
class QuoteFragment : Fragment() {

    companion object{
        private val PROFILE_QUOTE = "profileQuote"

        fun newInstance(profile: ProfileDao) : QuoteFragment {
            val args: Bundle = Bundle()
            args.putParcelable(PROFILE_QUOTE, profile)
            val fragment = QuoteFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.activity_main, container, false)

        val dao = arguments?.getParcelable<ProfileDao>(PROFILE_QUOTE) as ProfileDao;

        val buttonLike = view.findViewById<ImageView>(R.id.btn_love)
        val imageProfile = view.findViewById<ImageView>(R.id.image_view_profile)
        val txName = view.findViewById<TextView>(R.id.tx_name)
        val txLocalization = view.findViewById<TextView>(R.id.tx_localization)
        val txQuote = view.findViewById<TextView>(R.id.tx_quote)

        Picasso.get().load(dao.images[0]).into(imageProfile)
        txName.text = dao.name
        txLocalization.text = "${dao.city} / ${dao.state}"
        txQuote.text = "${dao.quotes[0].answer}\n${dao.quotes[1].answer}\n${dao.quotes[2].answer}"


        buttonLike.setOnClickListener {
            showDialog(dao)
        }

        imageProfile.setOnClickListener {
            val intent = Intent(activity, ProfileImageActivity::class.java)
            intent.putStringArrayListExtra("IMAGES", dao.images as ArrayList<String>)
            startActivity(intent)
        }

        return view
    }

    fun showDialog(profile : ProfileDao) {
        val bottomSheetDialogFragment = AnswerQuoteDialog.newInstance(profile)
        bottomSheetDialogFragment.show(activity?.supportFragmentManager, bottomSheetDialogFragment.getTag())
    }

}